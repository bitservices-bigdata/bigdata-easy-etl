FROM ruby:2.5.3-alpine3.8

ENV TZ UTC

RUN mkdir -p /opt/app
ADD . /opt/app
WORKDIR /opt/app

RUN bundle install --without development test

CMD ["ruby", "lib/main.rb"]