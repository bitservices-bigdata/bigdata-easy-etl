# Pipeline

This is a all pipeline interface to execute ETL.

## Parser

This pipe is used for translate string into hash

  {
    "type": "JSON",
    "options": {}
  }

### JSON

Translate String JSON to Hash

### JSONArray

Translate String Array JSON into many jsons. Where each json is a message

## Transformer

### Schema

Schema is a way to do transforms from data input. Use envvar SCHEMA to define a json with schema.

    {
      "type": "Schema",
      "options": {
        "id": {
          "from": "Personal.Id",
          "transform": "int"
        },
        "name": "Nome",
        "age": "Idade",
        "last_name": "Desc.last_name",
        "personal": {
          "embbeds": {
            "street": "Personal.Address",
            "number": {
              "from": "Personal.Number",
              "default": "s/n"
            }
          }
        },
        "products": {
          "from": "products",
          "type": "list",
          "embbeds": {
            "id": "_id",
            "name": "Name"
          }
        }
      }
    }

**Schema Format Example**

    {
      "id": {
        "from": "Personal.Id",
        "transform": "int"
      },
      "name": "Nome",
      "age": "Idade",
      "last_name": "Desc.last_name",
      "personal": {
        "embbeds": {
          "street": "Personal.Address",
          "number": {
            "from": "Personal.Number",
            "default": "s/n"
          }
        }
      },
      "products": {
        "from": "products",
        "type": "list",
        "embbeds": {
          "id": "_id",
          "name": "Name"
        }
      }
    }

**input**

    {
      "Nome": "Allan",
      "Idade": "45",
      "Desc": {
        "last_name": "Batista"
      },
      "Personal": {
        "Address": "rua ancora",
        "Id": "9203"
      },
      "products": [{
        "_id": 123,
        "Name": "product 1"
      }, {
        "_id": "124",
        "Name": "product 2"
      }]
    }

**output**

    {
      "id": 9203,
      "name": "Allan",
      "age": "45",
      "last_name": "Batista",
      "personal": {
        "street": "rua ancora",
        "number": "s/n"
      },
      "products": [
        {
          "id": 123,
          "name": "product 1"
        },
        {
          "id": "124",
          "name": "product 2"
        }
      ]
    }

## Consumers

Use envvar to configure consumers CONSUMER in json format

### RabbitMQ

    {
      "type": "RabbitMQ",
      "options": {
        "batch_size": 1,
        "queue_name": "",
        "host": "",
        "port": 5672,
        "vhost": "/",
        "user": "",
        "pass": ""
      }
    }

## Plublishers

Use envvar to configure publisher PUBLISHER in json format

### BigQuery

    {
      "type": "BigQuery",
      "options": {
        "project_id": "",
        "batch_size": 1000,
        "credentials":{
          "type": "",
          "project_id": "",
          "private_key_id": "",
          "private_key": "",
          "client_email": "",
          "client_id": "",
          "auth_uri": "",
          "token_uri": "",
          "auth_provider_x509_cert_url": "",
          "client_x509_cert_url": ""
        }
      }
    }

## Loggers

this is used to log errors to some place

### Elasticsearch

    {
      "type": "Elasticsearch",
      "options": {
      }
    }