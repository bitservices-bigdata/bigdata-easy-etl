require 'json'
require_relative "../../../../../spec/spec_helper"

describe EasyETL::Pipeline::Logger::Elasticsearch do
  let(:logger) { EasyETL::Pipeline::Logger::Elasticsearch.new }
  let(:errors) { [EasyETL::Pipeline::Error.new("a", "b", "c"), EasyETL::Pipeline::Error.new("a", "e", "f")] }
  let(:errors_with3) { [EasyETL::Pipeline::Error.new("a", "b", "c"), EasyETL::Pipeline::Error.new("a", "e", "f"), EasyETL::Pipeline::Error.new("a", "e", "f")] }
  let(:bulk_ndjson) { fixture("logger/elasticsearch/bulk.ndjson") }
  let(:bulk_ndjson_with3) { fixture("logger/elasticsearch/bulk_with3.ndjson") }

  context "#format_to_bulk" do
    it "should format currect bulk" do
      bulk = logger.send(:format_to_bulk, errors)
      expect(bulk).to eq(bulk_ndjson)
    end
  end

  context "#execute" do
    it "should success all" do
      @stubs.post("/_bulk", bulk_ndjson)  { |env| [ 200, {}, fixture("logger/elasticsearch/response_all_success.json") ]}

      result = logger.execute(errors)

      expect(result.errors).to eq([])
      expect(result.success).to eq([
        {"_index"=>"logs-2019.01.11", "_type"=>"log", "_id"=>"AWg9DELU7O5C6nEeOfOu", "_version"=>1, "_shards"=>{"total"=>2, "successful"=>1, "failed"=>0}, "status"=>201},
        {"_index"=>"logs-2019.01.11", "_type"=>"log", "_id"=>"AWg9DELU7O5C6nEeOfOv", "_version"=>1, "_shards"=>{"total"=>2, "successful"=>1, "failed"=>0}, "status"=>201}
      ])

      @stubs.verify_stubbed_calls
    end

    it "should has error" do
      @stubs.post("/_bulk", bulk_ndjson_with3)  { |env| [ 200, {}, fixture("logger/elasticsearch/response_with_error3.json") ]}

      result = logger.execute(errors_with3)

      expect(result.errors).to eq([{"_index"=>"logs-2019.01.11", "_type"=>"log", "_id"=>"AWg9DQrk7O5C6nEeOfOx", "status"=>400, "error"=>{"type"=>"mapper_parsing_exception", "reason"=>"failed to parse", "caused_by"=>{"type"=>"json_e_o_f_exception", "reason"=>"Unexpected end-of-input: was expecting closing '\"' for name\n at [Source: org.elasticsearch.common.io.stream.InputStreamStreamInput@1237e53e; line: 1, column: 73]"}}}, {"_index"=>"logs-2019.01.11", "_type"=>"log", "_id"=>"AWg9cjyh7O5C6nEeOfPG", "status"=>400, "error"=>{"type"=>"mapper_parsing_exception", "reason"=>"failed to parse [pipeline_name]", "caused_by"=>{"type"=>"illegal_argument_exception", "reason"=>"unknown property [pipeline_name]"}}}])
      expect(result.success).to eq([{"_index"=>"logs-2019.01.11", "_type"=>"log", "_id"=>"AWg9DQrk7O5C6nEeOfOw", "_version"=>1, "_shards"=>{"total"=>2, "successful"=>1, "failed"=>0}, "status"=>201}])

      @stubs.verify_stubbed_calls
    end

    it "should has 500 error" do
      @stubs.post("/_bulk", bulk_ndjson)  { |env| [ 500, {}, fixture("logger/elasticsearch/response_500.json") ]}

      expect(logger.send(:logger)).to receive(:exception)
      logger.execute(errors)

      @stubs.verify_stubbed_calls
    end
  end

end