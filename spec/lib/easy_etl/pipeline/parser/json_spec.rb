require 'json'
require_relative "../../../../../spec/spec_helper"

describe EasyETL::Pipeline::Parser::JSON do
  let(:parser) { EasyETL::Pipeline::Parser::JSON.new }

  it "should translate all with success" do
    result = parser.execute(['{"d": 1}', '{"c": 2}'])
    expect(result.success).to eq([{"d" => 1}, {"c" => 2}])
    expect(result.errors).to eq([])
  end

  it "should translate success and errors" do
    result = parser.execute(['{"d": 1}', '{"c: 2}', '{"d": 5}'])
    expect(result.success).to eq([{"d" => 1}, {"d" => 5}])
    expect(result.errors.size).to eq(1)
    expect(result.errors.first.raw).to eq('{"c: 2}')
    expect(result.errors.first.message).to eq("765: unexpected token at '{\"c: 2}'")
  end
end