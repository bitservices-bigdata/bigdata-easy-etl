require 'json'
require_relative "../../../../spec_helper"

describe EasyETL::Pipeline::Transformer::Schema do
  let(:schema1) { JSON.parse(fixture('schema/schema1.json')) }
  let(:schema1_simple_tokens) { JSON.parse(fixture('schema/schema1_simple_tokens.json')) }
  let(:message1) { JSON.parse(fixture('schema/message1.json')) }

  let(:schema1_join_arrays_to_hash) { JSON.parse(fixture('schema/schema1_join_arrays_to_hash.json')) }
  let(:message1_join_arrays_to_hash) { JSON.parse(fixture('schema/message1_join_arrays_to_hash.json')) }

  let(:schema_dates) { JSON.parse(fixture('schema/schema_dates.json')) }
  let(:message_dates) { JSON.parse(fixture('schema/message_dates.json')) }

  it "should not parse when no has any schema" do
    schema = EasyETL::Pipeline::Transformer::Schema.new(nil)

    expect(schema.translate(message1)).to eq(message1)
  end

  context "#translate" do
    it "simple names" do
      schema = EasyETL::Pipeline::Transformer::Schema.new(schema1)

      expect(
        schema.translate(message1).to_json
      ).to eq('{"id":9203,"name":"Allan","age":"45","last_name":"Batista","personal":{"street":"rua ancora","number":"s/n"},"products":[{"id":123,"name":"product 1"},{"id":"124","name":"product 2"}],"permit_null":null}')
    end

    it "use simple token as from" do
      schema = EasyETL::Pipeline::Transformer::Schema.new(schema1_simple_tokens)

      expect(
        schema.translate(message1).to_json
      ).to eq('{"id":9203,"name":"Allan","age":"45","last_name":"Batista","personal":{"street":"rua ancora","number":"s/n"},"products":[{"id":123,"name":"product 1"},{"id":"124","name":"product 2"}]}')
    end

    it "join arrays with one hash" do
      schema = EasyETL::Pipeline::Transformer::Schema.new(schema1_join_arrays_to_hash)

      expect(
        schema.translate(message1_join_arrays_to_hash).to_json
      ).to eq("{\"sellers\":[{\"name\":\"allan\",\"price\":150.0,\"outro\":111},{\"name\":\"batista\",\"price\":172.0,\"outro\":\"999\"},{\"price\":135.0,\"outro\":\"s/n\"}]}")
    end

    it "join arrays with one hash" do
      schema = EasyETL::Pipeline::Transformer::Schema.new({
        "codigo" => "codigo",
        "cep" => "cep",
        "nr_preco_frete" => "nr_preco_frete",
        "nr_prazo_frete" => "nr_prazo_frete",
        "bl_workday" => "bl_workday",
        "dt_visited" => "dt_visited",
        "product_availability" => "product_availability",
        "website_id" => "website_id",
        "variacao" => "variacao",
        "nome" => "nome",
        "partner_name" => "partner_name",
        "categoria_nivel1" => "categoria_nivel1",
        "categoria_nivel2" => "categoria_nivel2",
        "categoria_nivel3" => "categoria_nivel3",
        "product_price" => "product_price",
        "preco_por" => "preco_por",
        "preco_de" => "preco_de",
        "marketplace_main_seller" => "marketplace_main_seller",
        "marca" => "marca",
        "url" => "url",
        "original_url" => "original_url",
        "parcela_max_sj" => "parcela_max_sj",
        "b2w_sku" => "b2w_product.sku",
        "b2w_product_id" => "b2w_product.id",
        "sellers" => {
          "from" => ["marketplace_sellers", "marketplace_prices"],
          "type" => "list",
          "embbeds" => {
            "name" => "marketplace_sellers",
            "price" => "marketplace_prices"
          }
        },
        "trail_id" => "trail_id"
      })

      expect(
        schema.translate({
          "url" => "http://produto.casasbahia.com.br/3230331",
          "codigo" => "3230331",
          "cep" => "91410150",
          "nr_preco_frete" => 4.99,
          "nr_prazo_frete" => 2,
          "bl_workday" => true,
          "dt_visited" => "2018-12-08T00:49:21.482204",
          "product_availability" => true,
          "website_id" => 22,
          "variacao" => "110V ",
          "nome" => "Liquidificador Mondial Eletronic Filter Premium com 10 Velocidades Fun\u00e7\u00e3o Pulsar e Filtro 700W L-66 \u2013 Inox/Pret",
          "partner_name" => "Name Partner",
          "categoria_nivel1" => "Eletroport\u00e1teis",
          "categoria_nivel2" => "Liquidificadores e Acess\u00f3rios",
          "categoria_nivel3" => "Liquidificadores",
          "product_price" => 89.9,
          "preco_por" => 89.9,
          "preco_de" => 114.88,
          "marketplace_main_seller" => "CasasBahia.com.br",
          "marca" => "Mondial",
          "original_url" => "http://produto.casasbahia.com.br/3230331",
          "parcela_max_sj" => 2,
          "trail_id" => "1",
          "b2w_product" => {
            "id" => "123",
            "sku" => "sku123"
          },
          "marketplace_sellers" => [
            "Mondial Eletrodom\u00e9sticos",
            "Lojas Xavier"
          ],
          "marketplace_prices" => [
            139.9,
            129.0
          ]
        })
      ).to eq({
        "url" => "http://produto.casasbahia.com.br/3230331",
        "codigo" => "3230331",
        "cep" => "91410150",
        "nr_preco_frete" => 4.99,
        "nr_prazo_frete" => 2,
        "b2w_product_id" => "123",
        "b2w_sku" => "sku123",
        "bl_workday" => true,
        "dt_visited" => "2018-12-08T00:49:21.482204",
        "product_availability" => true,
        "website_id" => 22,
        "variacao" => "110V ",
        "nome" => "Liquidificador Mondial Eletronic Filter Premium com 10 Velocidades Fun\u00e7\u00e3o Pulsar e Filtro 700W L-66 \u2013 Inox/Pret",
        "partner_name" => "Name Partner",
        "categoria_nivel1" => "Eletroport\u00e1teis",
        "categoria_nivel2" => "Liquidificadores e Acess\u00f3rios",
        "categoria_nivel3" => "Liquidificadores",
        "product_price" => 89.9,
        "preco_por" => 89.9,
        "preco_de" => 114.88,
        "marketplace_main_seller" => "CasasBahia.com.br",
        "marca" => "Mondial",
        "original_url" => "http://produto.casasbahia.com.br/3230331",
        "parcela_max_sj" => 2,
        "trail_id" => "1",
        "sellers" => [
          {"name" => "Mondial Eletrodom\u00e9sticos", "price" => 139.9},
          {"name" => "Lojas Xavier", "price" => 129.0}
        ]
      })
    end

    it "should create value_normalized_to_float" do
      schema = EasyETL::Pipeline::Transformer::Schema.new({
        "all_conv_value" => {
          "from" => "all_conv_value",
          "transform" => "float"
        }, # "0.00",
        "total_conv_value" => {
          "from" => "total_conv_value",
          "transform" => "float"
        },
        "cost_div_conv" => {
          "from" => "cost_/_conv",
          "transform" => "value_normalized_to_float"
        },
        "cost_div_all_conv" => {
          "from" => "cost_/_all_conv",
          "transform" => "value_normalized_to_float"
        },
        "day_of_week" => "day_of_week",
        "clicks" => {
          "from" => "clicks",
          "transform" => "int"
        },
        "conversions" => {
          "from" => "conversions",
          "transform" => "float"
        },
        "all_conv" => {
          "from" => "all_conv",
          "transform" => "float"
        },
        "ctr" => {
          "from" => "ctr",
          "transform" => "float"
        },
        "impressions" => {
          "from" => "impressions",
          "transform" => "int"
        },
        "condition" => "condition",
        "all_conv_rate" => {
          "from" => "all_conv_rate",
          "transform" => "float"
        },
        "conv_rate" => {
          "from" => "conv_rate",
          "transform" => "float"
        },
        "cost" => {
          "from" => "cost",
          "transform" => "value_normalized_to_float"
        },
        "value_div_all_conv" => {
          "from" => "value_/_all_conv",
          "transform" => "float"
        },
        "value_div_conv" => {
          "from" => "value_/_conv",
          "transform" => "float"
        },
        "item_id" =>  "item_id",
        "google_extract_time" => {
          "from" => "google_extract_time",
          "transform" => "milliseconds_to_iso8601"
        },
        "month" => "month", # "2018-12-01",
        "device" => "device", # "Mobile devices with full browsers",
        "brand" => "brand", # "ACOM",
        "account" => "account", # "ACOM - CONTA 02 (PLA)",
        "avg_cpc" => {
          "from" => "avg_cpc",
          "transform" => "value_normalized_to_float"
        },
        "day" => {
          "from" => "day",
          "transform" => "date_to_iso8601"
        },
        "line_identifier" => {
          "type" => "sha2"
        } 
      })

      expect(
        schema.translate({
          "impressions" => "2",
          "condition" => "New",
          "all_conv_value" => "0.00",
          "total_conv_value" => "0.00",
          "cost_/_conv" => "10000000",
          "conv_rate" => "0.00%",
          "all_conv_rate" => "0.00%",
          "cost_/_all_conv" => "0",
          "cost" => "0",
          "value_/_all_conv" => "0.00",
          "day_of_week" => "Thursday",
          "value_/_conv" => "0.00",
          "item_id" => "48099021",
          "google_extract_time" => "1547175821716",
          "clicks" => "0",
          "month" => "2019-01-01",
          "device" => "Mobile devices with full browsers",
          "avg_cpc" => "0",
          "account" => "ACOM - CONTA 02 (PLA)",
          "brand" => "ACOM",
          "day" => "2019-01-10",
          "conversions" => "0.00",
          "ctr" => "0.00%",
          "all_conv" => "0.00"
        })
      ).to eq({"all_conv_value"=>0.0, "total_conv_value"=>0.0, "cost_div_conv"=>10.0, "cost_div_all_conv"=>0.0, "day_of_week"=>"Thursday", "clicks"=>0, "conversions"=>0.0, "all_conv"=>0.0, "ctr"=>0.0, "impressions"=>2, "condition"=>"New", "all_conv_rate"=>0.0, "conv_rate"=>0.0, "cost"=>0.0, "value_div_all_conv"=>0.0, "value_div_conv"=>0.0, "item_id"=>"48099021", "google_extract_time"=>"2019-01-11T03:03:41+00:00", "month"=>"2019-01-01", "device"=>"Mobile devices with full browsers", "brand"=>"ACOM", "account"=>"ACOM - CONTA 02 (PLA)", "avg_cpc"=>0.0, "day"=>"2019-01-10T00:00:00+00:00", "line_identifier"=>"0b0b5f31fa1c267c0dc95df41f6ecf4c3310574c1c62806e05b8494f26fc3f34"})
    end

    it "should implement lambda from value" do
      schema = EasyETL::Pipeline::Transformer::Schema.new({
        "data" => {
          "from" => "d",
          "lambda" => "lambda {|v| v.to_i * 2 }"
        },
        "price_to" => {
          "from" => "casa.super.price",
          "lambda" => "lambda {|v| (v.to_f * 0.9).round(2) }"
        },
        "should_be_null" => {
          "from" => "casa.super.price",
          "lambda" => "lambda {|v| 1 / 0 }",
          "permit_null" => true
        },
        "publisher" => {
          "from_lambda" => 'lambda do |config, data| return data["publisher"] unless data["publisher"].nil?;     referer = data["redirect_url"] || data["referer"] || data["headers"]["referer"] rescue nil;     referer = referer.to_s;     return "americanas" if referer.include?("americanas.com.br") || referer.include?("google.com");     return "submarino" if referer.include?("submarino.com.br");     return "shoptime" if referer.include?("shoptime.com.br"); end',
          "permit_null" => true
        }
      })

      expect(schema.translate({
        "d" => "2",
        "casa" => {
          "super" => {
            "price" => "199.88"
          }
        },
        "redirect_url" => "https://www.google.com.br",
        "referer" => "https://www.submarino.com.br",
        "publisher" => "americanas",
        "headers" => {
          "referer" => "https://www.shoptime.com.br"
        }
      })).to eq({
        "data" => 4,
        "price_to" => 179.89,
        "publisher" => "americanas",
        "should_be_null" => nil
      })

      expect(schema.translate({
        "d" => "2",
        "casa" => {
          "super" => {
            "price" => "199.88"
          }
        },
        "redirect_url" => "https://www.google.com.br",
        "referer" => "https://www.submarino.com.br",
        "headers" => {
          "referer" => "https://www.shoptime.com.br"
        }
      })).to eq({
        "data" => 4,
        "price_to" => 179.89,
        "publisher" => "americanas",
        "should_be_null" => nil
      })

      expect(schema.translate({
        "referer" => "https://www.submarino.com.br",
        "headers" => {
          "referer" => "https://www.shoptime.com.br"
        }
      })).to eq({
        "data" => 0,
        "price_to" => 0.0,
        "publisher" => "submarino",
        "should_be_null" => nil
      })

      expect(schema.translate({
        "headers" => {
          "referer" => "https://www.shoptime.com.br"
        }
      })).to eq({
        "data" => 0,
        "price_to" => 0.0,
        "publisher" => "shoptime",
        "should_be_null" => nil
      })

      expect(schema.translate({})).to eq({
        "data" => 0,
        "price_to" => 0.0,
        "publisher" => nil,
        "should_be_null" => nil
      })
    end

    it "should translate custom parses" do
      schema = EasyETL::Pipeline::Transformer::Schema.new(schema_dates)

      expect(schema.translate(message_dates)).to eq({
        "date1"=>"2019-02-05T11:23:49-02:00",
        "date2"=>"2019-02-05T09:19:21+00:00",
        "date3"=>"2019-02-01T16:14:47+00:00",
        "date4"=>"2019-02-05T09:19:21-02:00",
        "date5"=>"2019-01-11T11:06:47-02:00"
      })
    end

    it "should create raw field" do
      schema = EasyETL::Pipeline::Transformer::Schema.new({
        "data" => {
          "type" => "raw",
        }
      })

      expect(schema.translate({
        "d" => "2",
        "casa" => {
          "super" => {
            "price" => "199.88"
          }
        }
      })).to eq({"data"=>"{\"d\":\"2\",\"casa\":{\"super\":{\"price\":\"199.88\"}}}"})
    end
  end

  context "#execute" do
    it "translate many with all success" do
      schema = EasyETL::Pipeline::Transformer::Schema.new(schema1)
      result = schema.execute([message1, message1])
      expect(result.success.to_json).to eq('[{"id":9203,"name":"Allan","age":"45","last_name":"Batista","personal":{"street":"rua ancora","number":"s/n"},"products":[{"id":123,"name":"product 1"},{"id":"124","name":"product 2"}],"permit_null":null},{"id":9203,"name":"Allan","age":"45","last_name":"Batista","personal":{"street":"rua ancora","number":"s/n"},"products":[{"id":123,"name":"product 1"},{"id":"124","name":"product 2"}],"permit_null":null}]')
    end
  end
end