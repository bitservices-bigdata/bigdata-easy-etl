require 'json'
require_relative "../../../spec_helper"

describe EasyETL::Pipeline::Error do
  it "should convert to hash currect" do
    error = EasyETL::Pipeline::Error.new("EasyETL::Pipeline::Error", "error message", "body")
    
    expect(error.to_log).to eq({
      pipeline_name: "EasyETL::Pipeline::Error",
      message: "error message",
      raw: "body",
      created_at: DateTime.parse("2019-01-11T11:06:47-02:00")
    })
  end

  it "should convert connect types messesage to string" do
    expect(EasyETL::Pipeline::Error.new("EasyETL::Pipeline::Error", {message: "asd"}, 1).to_log).to eq({
      pipeline_name: "EasyETL::Pipeline::Error",
      message: "{\"message\":\"asd\"}",
      raw: "1",
      created_at: DateTime.parse("2019-01-11T11:06:47-02:00")
    })

    expect(EasyETL::Pipeline::Error.new("EasyETL::Pipeline::Error", true, [{a: 1}]).to_log).to eq({
      pipeline_name: "EasyETL::Pipeline::Error",
      message: "true",
      raw: "[{\"a\":1}]",
      created_at: DateTime.parse("2019-01-11T11:06:47-02:00")
    })
  end
end