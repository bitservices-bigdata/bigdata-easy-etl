require 'json'

module EasyETL::Helpers
  APP_ENV = {
    "PIPELINE_NAME" => "default",

    'PUBLISHER'   => '{"type": "Output", "options": {"sleep_time": 0.5}}',
    'CONSUMER'    => '{"type": "RabbitMQ", "options": {"queue_name": "teste", "batch_size": 10, "sleep_time": 1.0}}',
    'LOGGER'      => '{"type": "Elasticsearch", "options": {}}',
    'PARSER'      => '{"type": "JSON", "options": {}}',
    'TRANSFORMER' => '{"type": "Schema", "options": {}}',
  
    'ENVIRONMENT' => 'development',
    "LOG_LEVEL" => "info"
  }
  
  APP_ENV.keys.each do |key|
    APP_ENV[key] = ENV[key] if ENV[key]
    APP_ENV[key] = JSON.parse(APP_ENV[key]) rescue APP_ENV[key]
  end
end