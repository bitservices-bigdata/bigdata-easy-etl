module EasyETL::RequestError
  class ServerError < StandardError
    attr_reader :body, :response
  
    def initialize(body, response)
      @body = body
      @response = response
  
      super(response.body)
    end
  end
end