require_relative "../../logger"
require_relative "../result"

class EasyETL::Pipeline::Transformer::Base
  attr_reader :options

  def initialize(options = {})
    @options = options || {}
    @logger = EasyETL::Logger.new(self.class.name)
    @logger.info("start")
  end

  protected

  def logger
    @logger
  end
end