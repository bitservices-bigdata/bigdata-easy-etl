require_relative 'schema'

class EasyETL::Pipeline::Transformer::Builder
  class << self
    def build(options)
      Object.const_get("EasyETL::Pipeline::Transformer::#{options["type"]}").new(options["options"])
    end
  end
end