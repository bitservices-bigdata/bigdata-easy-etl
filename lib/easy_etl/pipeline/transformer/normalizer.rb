require 'time'

class EasyETL::Pipeline::Transformer::Normalizer < EasyETL::Pipeline::Transformer::Base
  class << self
    @@basic_types = {
      'date' => {
        'iso8601' => '%Y-%m-%dT%H:%M:%S%:z',
        'simple_datetime' => '%Y-%m-%d %H:%M:%S'
      }
    }

    def normalize(type, value)
      return nil if value.nil?

      if type.is_a?(Hash)
        return custom_normalize(type, value)
      end

      case type
        when "int"
          value.to_i
        when "float"
          value.to_f
        when "str"
          value.to_s
        when "milliseconds_to_iso8601"
          Time.at(value.to_i/1000).to_datetime.to_s
        when "date_to_iso8601"
          DateTime.parse(value).to_s
        when "value_normalized_to_float"
          value.to_f / 1_000_000.0
        else
          value
      end
    end

    def custom_normalize(type, value)
      case type["type"]
      when "date"
        normalize_date(type, value)
      else
        value
      end
    end

    def normalize_date(type, value)
      from_format = get_basic_type(type["type"], type["from"]) || type["from"]
      to_format = get_basic_type(type["type"], type["to"]) || type["to"]
      Time.strptime(value, from_format).strftime(to_format)
    end

    def get_basic_type(type, name)
      @@basic_types[type][name]
    end
  end
end