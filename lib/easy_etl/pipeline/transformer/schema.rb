require 'json'
require_relative 'base'
require_relative 'normalizer'
require_relative '../result'

class EasyETL::Pipeline::Transformer::Schema < EasyETL::Pipeline::Transformer::Base
  attr_reader :schema

  def initialize(schema)
    @schema = schema

    @schema.keys.each do |key|
      @schema[key]["embbeds"] = EasyETL::Pipeline::Transformer::Schema.new(@schema[key]["embbeds"]) if @schema[key].include?("embbeds")
      @schema[key]["lambda"] = eval(@schema[key]["lambda"]) if @schema[key].include?("lambda")
      @schema[key]["from_lambda"] = eval(@schema[key]["from_lambda"]) if @schema[key].include?("from_lambda")
    end if @schema.is_a?(Hash)
    
    super
  end

  def translate(data)
    return data if !@schema.is_a?(Hash)
    do_translate(data)
  end

  def execute(chunk)
    if execute?
      logger.info("skip")
      return EasyETL::Pipeline::Result.new(chunk, [])
    end
    success = []
    errors = []

    chunk.each do |data|
      begin
        success << do_translate(data)
      rescue => exception
        errors << EasyETL::Pipeline::Error.new(self.class.name, exception.message, data)
      end
    end

    logger.info("success #{chunk.size - errors.size} / errors #{errors.size}")
    EasyETL::Pipeline::Result.new(success, errors)
  end

  private

  # se não for um hash ou estiver vazio
  def execute?
    !@schema.is_a?(Hash) || @schema.empty?
  end

  def do_translate(data)
    newdata = {}

    @schema.keys.each do |key|
      value = parse_dest_value(@schema[key], data)
      newdata[key] = value if !value.nil? || @schema[key]["permit_null"]
    end

    newdata
  end

  def get_config(key)
    @schema[key]
  end

  def get_dest_name(original_name, config)
    config["dest"] || original_name
  end

  def parse_dest_value(config, data)
    if config["type"] == "list"
      get_values_by_path(config, data)
    elsif !config["embbeds"].nil?
      config["embbeds"].translate(data)
    else
      get_value_by_path(config, data)
    end
  end

  def normalize_value(config, value)
    EasyETL::Pipeline::Transformer::Normalizer.normalize(config["transform"], value)
  rescue => e
    raise e if config["raise_error"]
  end

  def get_value_by_path(config, data)
    if config["from"].is_a?(Array)
      _get_values_by_path(config, data)
    else
      _get_value_by_path(config, data)
    end
  end

  def _get_values_by_path(config, data)
    values = config["from"].map do |from|
      _config = config.clone
      _config["from"] = from
      _get_value_by_path(_config, data) || []
    end

    create_temp_hash_values(config, values)
  end

  def create_temp_hash_values(config, values)
    max_values_size(values).times.map do |index|
      value = {}
      
      config["from"].size.times do |i|
        keyspath = config["from"][i].split(".")
        last_key = keyspath.pop
        _value = value
        keyspath.each do |key|
          _value[key] = {}
          _value = _value[key]
        end
        _value[last_key] = values[i][index]
      end
      
      value
    end
  end

  def max_values_size(values)
    values.map {|v| v.size}.max
  end

  def _get_value_by_path(config, data)    
    if config['from_lambda'].nil?
      value = data

      from = config.is_a?(String) ? config : config["from"]

      return create_field(config, data) if from.nil?

      from.split(".").each do |key|
        begin
          value = value[key] 
        rescue => e
          value = nil
          break
        end
      end
    else
      value = config['from_lambda'].call(config, data)
    end
    
    value = normalize_value(config, value || config["default"])
    lambda_value(config, value)
  end

  def create_field(config, data)
    case config["type"]
    when "sha2"
      Digest::SHA2.hexdigest(data.to_json)
    when "iso8601_now"
      DateTime.now.to_s
    when "raw"
      data.to_json
    end
  end

  def lambda_value(config, value)
    return value if config["lambda"].nil?
    config["lambda"].call(value)
  rescue => e
    raise e if config["raise_error"]
  end

  def get_values_by_path(config, data)
    get_value_by_path(config, data).map do |value|
      config["embbeds"].translate(value)
    end
  end
end