require_relative 'base'

class EasyETL::Pipeline::Publisher::Output < EasyETL::Pipeline::Publisher::Base
  def execute(chunck)
    chunck.each do |message|
      print(message)
      wait
    end

    EasyETL::Pipeline::Result.new(nil, [])
  end

  def print(message)
    puts message
  end

  def wait
    sleep options["sleep_time"] if sleep?
  end

  def sleep?
    @sleep ||= options["sleep_time"].to_f >= 0.0
  end
end