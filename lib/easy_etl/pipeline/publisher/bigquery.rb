require_relative 'base'
require "google/cloud/bigquery"
require 'base64'

class EasyETL::Pipeline::Publisher::BigQuery < EasyETL::Pipeline::Publisher::Base
  def execute(chunck)
    errors = []

    chunck.each_slice(batch_size) do |rows|
      response = table.insert(rows, skip_invalid: true)
      errors << parse_errors(response, rows)
    end
    
    errors = errors.flatten

    logger.info("success #{chunck.size - errors.size} / errors #{errors.size}")
    EasyETL::Pipeline::Result.new(nil, errors)
  end

  def dataset
    @dataset ||= connection.dataset(dataset_name)
  end

  def table
    @table ||= dataset.table(table_name)
  end

  private

  def parse_errors(response, chunck)
    response.insert_errors.map do |error|
      EasyETL::Pipeline::Error.new(self.class.name, "#{error.row.to_json}\n#{error.errors.to_json}", chunck[error.index])
    end
  end

  def batch_size
    @batch_size ||= options["batch_size"] || 1000
  end

  def dataset_name
    options["dataset_name"]
  end

  def table_name
    options["table_name"]
  end

  def connection
    @connection ||= Google::Cloud::Bigquery.new(
      project_id: options["project_id"],
      credentials: JSON.parse(Base64.urlsafe_decode64(options["credentials"]))  
    )
  end
end