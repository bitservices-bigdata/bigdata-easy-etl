require_relative 'bigquery'
require_relative 'output'

class EasyETL::Pipeline::Publisher::Builder
  class << self
    def build(options)
      Object.const_get("EasyETL::Pipeline::Publisher::#{options["type"]}").new(options["options"])
    end
  end
end