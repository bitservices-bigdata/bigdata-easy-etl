require_relative "../../logger"

class EasyETL::Pipeline::Logger::Base
  attr_reader :options

  def initialize(options = {})
    @options = options || {}
    @logger = EasyETL::Logger.new(self.class.name)
    logger.info("start")
  end

  protected

  def logger
    @logger
  end
end