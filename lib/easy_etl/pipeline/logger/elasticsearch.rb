require 'time'
require 'faraday'
require_relative "base"
require_relative "../../logger"
require_relative "../../request_error"

class EasyETL::Pipeline::Logger::Elasticsearch < EasyETL::Pipeline::Logger::Base
  attr_reader :host, :index_name, :document_name

  def initialize(options={})
    @host = options["host"] || "http://localhost:9200"
    @index_name = options["index_name"] || "logs"
    @document_name = options["document_name"] || "log"
    @pipeline_name = options["pipeline_name"] || "default"
    super
  end

  def execute(chunk)
    success = []
    errors  = []

    if chunk.empty?
      logger.info("skip")
      return EasyETL::Pipeline::Result.new(success, errors)
    end

    formated = format_to_bulk(chunk)

    response = connection.post do |builder|
      builder.url "/_bulk"
      builder.headers['Content-Type'] = "application/x-ndjson"
      builder.body = formated
    end

    raise EasyETL::RequestError::ServerError.new(formated, response) if response.status >= 500

    parsed = JSON.parse(response.body)

    parsed["items"].each do |item|
      if item["create"]['status'] == 201
        success << item["create"]
      else
        errors << item["create"]
      end
    end

    logger.info("success #{success.size} / errors #{errors.size}")
    EasyETL::Pipeline::Result.new(success, errors)
  rescue => e
    logger.exception(e)
  end

  private

  def format_to_bulk(chunk)
    formated = chunk.map{|error| create_bulk_entry(error) }.flatten
    formated << "" # this is necessary because the interface of bulk on ES.
    formated.join("\n")
  end

  def create_bulk_entry(error)
    [
      create_bulk_index(error.created_at),
      error.to_log.to_json
    ]
  end

  def create_bulk_index(datetime=DateTime.now)
    {index: {_index: index_name_formated(datetime), _type: document_name}}.to_json
  end

  def connection
    @connection ||= Faraday.new(@host)
  end

  def index_name_formated(datetime=DateTime.now)
    "#{@index_name}-#{datetime.strftime("%Y.%m.%d")}"
  end
end