require_relative 'elasticsearch'

class EasyETL::Pipeline::Logger::Builder
  class << self
    def build(options)
      Object.const_get("EasyETL::Pipeline::Logger::#{options["type"]}").new(options["options"])
    end
  end
end