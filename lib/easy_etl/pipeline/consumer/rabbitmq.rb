require 'bunny'
require_relative 'base'

class EasyETL::Pipeline::Consumer::RabbitMQ < EasyETL::Pipeline::Consumer::Base
  def execute
    connection = create_connection()
    logger.info("Start connection #{connection}")
    connection.start
    channel = connection.create_channel()
    channel.prefetch(batch_size)
    queue = channel.queue(queue_name, durable: true, arguments: queue_arguments)

    @messages = []
    
    consumer = queue.subscribe(manual_ack: true) do |delivery_info, properties, payload|
      @messages << [delivery_info, properties, payload]
    end

    loop do
      break if stop?
      
      Signal.trap('INT') do
        if stop?
          puts "force stop! :("
          exit
        else
          puts "stopping gracely. wait please. :)"
          stop!
        end
      end

      sleep(sleep_time)

      if @messages.empty?
        logger.info("messages empty. sleeping #{sleep_time}s")
        next
      end

      messages, @messages = @messages, []
      
      yield(messages.map{|message| message[2]})
      channel.ack(messages.last[0].delivery_tag, true)
      logger.info("ack #{messages.size} messages")
    end
  rescue => e
    puts e.message
    logger.exception_and_raise(e)
  ensure
    logger.info("stop consumer")
    consumer.cancel rescue nil
    logger.info("close connection")
    connection.close rescue nil
  end

  def stop!
    @stop = true
    puts "to stop"
  end

  def stop?
    @stop == true
  end

  def batch_size
    @batch_size ||= @options["batch_size"] || 1000
  end

  def queue_name
    @queue_name ||= @options["queue_name"]
  end

  def sleep_time
    @sleep_time ||= @options["sleep_time"] || 0.2
  end

  def queue_arguments
    @queue_arguments ||= @options["queue_arguments"] || {}
  end

  private

  def create_connection
    Bunny.new({
      host: options["host"],
      port: options["port"] || 5672,
      vhost: options["vhost"] || "/",
      user: options["user"],
      password: options["pass"]
    })
  end
end