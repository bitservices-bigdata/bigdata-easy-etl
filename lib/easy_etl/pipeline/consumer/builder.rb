require_relative 'rabbitmq'

class EasyETL::Pipeline::Consumer::Builder
  class << self
    def build(options)
      Object.const_get("EasyETL::Pipeline::Consumer::#{options["type"]}").new(options["options"])
    end
  end
end