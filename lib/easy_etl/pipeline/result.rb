class EasyETL::Pipeline::Result
  attr_reader :success, :errors
  
  def initialize(success, errors)
    @success = success
    @errors = errors
  end
end