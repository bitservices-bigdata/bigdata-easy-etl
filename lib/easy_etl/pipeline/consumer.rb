module EasyETL
  module Pipeline
    module Consumer
      require_relative 'consumer/builder'
      require_relative 'consumer/rabbitmq'
    end
  end
end