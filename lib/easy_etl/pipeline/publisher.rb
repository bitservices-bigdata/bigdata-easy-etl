module EasyETL::Pipeline::Publisher
  require_relative "publisher/base"
  require_relative "publisher/bigquery"
  require_relative "publisher/builder"
end