require_relative 'json'
require_relative 'json_array'

class EasyETL::Pipeline::Parser::Builder
  class << self
    def build(options)
      Object.const_get("EasyETL::Pipeline::Parser::#{options["type"]}").new(options["options"])
    end
  end
end