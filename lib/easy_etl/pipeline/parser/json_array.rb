require 'json'
require_relative "base"
require_relative "../result"

class EasyETL::Pipeline::Parser::JSONArray < EasyETL::Pipeline::Parser::Base
  def execute(str_list)
    success = []
    errors  = []

    str_list.each do |str|
      begin
        success << JSON.parse(str)
      rescue => exception
        errors << EasyETL::Pipeline::Error.new(self.class.name, exception.message, str)
      end
    end

    logger.info("success #{str_list.size - errors.size} / errors #{errors.size}")
    EasyETL::Pipeline::Result.new(success.flatten, errors)
  end
end