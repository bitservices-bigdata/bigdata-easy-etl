module EasyETL
  module Pipeline
    module Parser
      require_relative 'parser/base'
      require_relative 'parser/json'
      require_relative 'parser/json_array'
      require_relative 'parser/builder'
    end
  end
end