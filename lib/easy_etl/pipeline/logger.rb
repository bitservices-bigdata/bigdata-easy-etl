module EasyETL
  module Pipeline
    module Logger
      require_relative 'logger/base'
      require_relative 'logger/elasticsearch'
      require_relative 'logger/builder'
    end
  end
end