require 'time'
require 'json'

module EasyETL
  module Pipeline
    class Error
      attr_reader :pipeline_name, :message, :raw, :created_at

      def initialize(pipeline_name, message, raw, created_at=DateTime.now)
        @pipeline_name = pipeline_name
        @message = message
        @raw = raw
        @created_at = created_at
      end

      def to_log
        {
          pipeline_name: pipeline_name,
          message: format_data(message),
          raw: format_data(raw),
          created_at: created_at
        }
      end
      
      def format_data(data)
        data.is_a?(Hash) || data.is_a?(Array) ? data.to_json : data.to_s
      end
    end
  end
end