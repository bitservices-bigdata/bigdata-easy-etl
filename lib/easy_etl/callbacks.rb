require_relative 'helpers'

module EasyETL
  class Callbacks
    def before_publish(chunck)
      chunck
    end

    def after_publish(publish_result)
      publish_result
    end

    eval(Helpers::APP_ENV['callbacks'].to_s)
  end
end