require 'logger'
require_relative 'helpers'

module EasyETL
  class Logger < ::Logger
    def initialize(class_name, lvl=Helpers::APP_ENV["LOG_LEVEL"])
      @class_name = class_name
      set_level(lvl)
      super(STDOUT, formatter: proc {|severity, datetime, progname, msg|
        "[#{class_name}][#{datetime.strftime('%Y-%m-%dT%H:%M:%S.%L%z')}] #{msg}\n"
      })
    end

    def exception(e)
      error("#{e.message}\n#{e.backtrace.join("\n")}")
    end

    def exception_and_raise(e)
      exception(e)
      raise e
    end

    def set_level(lvl)
      case lvl.to_s.downcase.strip
      when "warn"
        self.level = ::Logger::WARN
      when "error"
        self.level = ::Logger::ERROR
      when "fatal"
        self.level = ::Logger::FATAL
      when "debug"
        self.level = ::Logger::DEBUG
      else
        self.level = ::Logger::INFO
      end
    end
  end
end