require_relative 'easy_etl'
require_relative 'boot'

$logger.info("Starting")
$logger.info(EasyETL::Helpers::APP_ENV)

parser = EasyETL::Pipeline::Parser::Builder.build(EasyETL::Helpers::APP_ENV['PARSER'])
schema = EasyETL::Pipeline::Transformer::Builder.build(EasyETL::Helpers::APP_ENV['TRANSFORMER'])
consumer = EasyETL::Pipeline::Consumer::Builder.build(EasyETL::Helpers::APP_ENV['CONSUMER'])
publisher = EasyETL::Pipeline::Publisher::Builder.build(EasyETL::Helpers::APP_ENV['PUBLISHER'])
logger = EasyETL::Pipeline::Logger::Builder.build(EasyETL::Helpers::APP_ENV['LOGGER'])

consumer.execute do |chunck|
  parse_result = parser.execute(chunck)
  schema_result = schema.execute(parse_result.success)
  publish_result = publisher.execute(schema_result.success)
  logger.execute((parse_result.errors+schema_result.errors+publish_result.errors))
end